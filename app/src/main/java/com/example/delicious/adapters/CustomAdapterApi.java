package com.example.delicious.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.delicious.DataModel;
import com.example.delicious.R;

import java.util.ArrayList;

public class CustomAdapterApi extends RecyclerView.Adapter<CustomAdapterApi.MyViewHolder> {

    private ArrayList<DataModel> dataset;

    public CustomAdapterApi(ArrayList<DataModel> dataSet) {
        this.dataset = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewDescription
                ;
        TextView textViewTime;
        ImageView imageViewRecipe;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewNameSearchCard);
            textViewDescription = itemView.findViewById(R.id.textViewDescriptionSearchCard);
            textViewTime = itemView.findViewById(R.id.textViewTimeSearchCard);
            imageViewRecipe = itemView.findViewById(R.id.imageViewSearchCard);
        }
    }

    @NonNull
    @Override
    public CustomAdapterApi.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout_search, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterApi.MyViewHolder holder, int position) {
        holder.textViewName.setText(dataset.get(position).getName());
        holder.textViewTime.setText(dataset.get(position).getCategory());
        holder.textViewDescription.setText(dataset.get(position).getDescription());

        String imageUrl = dataset.get(position).getImageUrl();
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Glide.with(holder.itemView.getContext())
                    .load(imageUrl)
                    .into(holder.imageViewRecipe);
        } else {
            // Handle case where image URL is empty or null
            // You can set a default image here or hide the ImageView
        }
    }

    private Bitmap decodeImageFromString(String imageString) {
        try {
            byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}