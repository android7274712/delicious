package com.example.delicious;

import android.os.StrictMode;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DataService {
    private static   ArrayList<Recipe> Recipes = new ArrayList<>();
    private static String apiKey = "44e16cfbd1d3416da9c690b4eeb4af71";
    public static ArrayList<Recipe> getArrRecipes(String q) throws IOException{

        String sURL = "https://api.spoonacular.com/recipes/complexSearch?apiKey=" + apiKey +
                "&addRecipeInformation=true&instructionsRequired=true&fillIngredients=true&number=100&offset=0&query=" + q;


        JsonArray resultsArray = getJsonObject(sURL).get("results").getAsJsonArray();
        for (JsonElement result : resultsArray) {
            JsonObject recipeObj = result.getAsJsonObject();
            String name = recipeObj.get("title").getAsString();
            int totalTime = recipeObj.get("readyInMinutes").getAsInt();
            String category = ""; // You may need to derive this from other available data or APIs
            String description = recipeObj.get("summary").getAsString();
            String imageUrl = recipeObj.get("image").getAsString();
            ArrayList<String> ingredientsList = new ArrayList<>();
            JsonArray ingredientsArray = recipeObj.get("extendedIngredients").getAsJsonArray();
            for (JsonElement ingredient : ingredientsArray) {
                String ingredientName = ingredient.getAsJsonObject().get("name").getAsString();
                ingredientsList.add(ingredientName);
            }
            String steps = ""; // Spoonacular does not provide detailed steps directly in this endpoint

            Recipe recipe = new Recipe(name, String.valueOf(totalTime), category, description, ingredientsList, steps, imageUrl);
            Recipes.add(recipe);
        }

        return Recipes;
    }

    private static JsonObject getJsonObject(String sURL) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        URL url = new URL(sURL);
        HttpURLConnection request = (HttpsURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        return root.getAsJsonObject();
    }

    public static ArrayList<Recipe> getRandomArrRecipes() throws IOException{

        String sURL = "https://api.spoonacular.com/recipes/random?apiKey=" + apiKey + "&addRecipeInformation=true&instructionsRequired=true&fillIngredients=true&number=100&offset=0";
        JsonArray resultsArray = getJsonObject(sURL).get("recipes").getAsJsonArray();
        for (JsonElement result : resultsArray) {
            JsonObject recipeObj = result.getAsJsonObject();
            String name = recipeObj.get("title").getAsString();
            int totalTime = recipeObj.get("readyInMinutes").getAsInt();
            String category = ""; // You may need to derive this from other available data or APIs
            String description = recipeObj.get("summary").getAsString();
            String imageUrl = recipeObj.get("image").getAsString();
            ArrayList<String> ingredientsList = new ArrayList<>();
            JsonArray ingredientsArray = recipeObj.get("extendedIngredients").getAsJsonArray();
            for (JsonElement ingredient : ingredientsArray) {
                String ingredientName = ingredient.getAsJsonObject().get("name").getAsString();
                ingredientsList.add(ingredientName);
            }
            String steps = ""; // Spoonacular does not provide detailed steps directly in this endpoint

            Recipe recipe = new Recipe(name, String.valueOf(totalTime), category, description, ingredientsList, steps, imageUrl);
            Recipes.add(recipe);
        }

        return Recipes;
    }
}

