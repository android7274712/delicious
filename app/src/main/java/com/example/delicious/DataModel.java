package com.example.delicious;

public class DataModel {
    private String name;
    private String description;
    private String category;
    private String imageUrl;
    public DataModel(String name,String category,String description, String imageUrl){
        this.name = name;
        this.category = category;
        this.description = description;
        this.imageUrl = imageUrl;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String name) {
        this.category = category;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {this.imageUrl = imageUrl; }

}
